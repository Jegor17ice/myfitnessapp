package ru.ei.myFitnessApp.utils;

import ru.ei.myFitnessApp.controllers.responses.UserExerciseResponse;
import ru.ei.myFitnessApp.controllers.responses.UserFitnessStageResponse;
import ru.ei.myFitnessApp.db.entities.UserExercise;
import ru.ei.myFitnessApp.db.entities.UserFitnessStage;

public class ResponseUtils {
    public static UserFitnessStageResponse getStageResponse(UserFitnessStage stage) {
        return UserFitnessStageResponse.
                builder().
                stageID(stage.getId()).
                name(stage.getName()).
                description(stage.getDescription()).
                userID(stage.getUser().getId()).
                build();
    }


    public static UserExerciseResponse getUserExerciseResponse(UserExercise exercise) {
        return UserExerciseResponse.builder().id(exercise.getId()).
                name(exercise.getName()).
                description(exercise.getDescription()).
                number(exercise.getNumber()).
                stageID(exercise.getUserFitnessStage().getId()).
                executionTime(exercise.getExecutionTime()).
                missing(exercise.getMissing()).
                build();
    }
}
