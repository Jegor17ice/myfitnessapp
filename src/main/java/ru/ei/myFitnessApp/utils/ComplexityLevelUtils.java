package ru.ei.myFitnessApp.utils;

import ru.ei.myFitnessApp.db.entities.ComplexityLevel;
import ru.ei.myFitnessApp.utils.exceptions.BadArgumentException;

public class ComplexityLevelUtils {
    public static ComplexityLevel getComplexityLevel(String complexityLevel) throws BadArgumentException {

        for (ComplexityLevel level : ComplexityLevel.values()) {

            if (level.toString().equals(complexityLevel)) {

                return level;

            }

        }

        throw new BadArgumentException();
    }
}
