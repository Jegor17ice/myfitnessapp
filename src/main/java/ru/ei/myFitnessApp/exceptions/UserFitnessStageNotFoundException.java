package ru.ei.myFitnessApp.exceptions;

public class UserFitnessStageNotFoundException extends Exception {
    public UserFitnessStageNotFoundException() {
    }

    public UserFitnessStageNotFoundException(String message) {
        super(message);
    }

    public UserFitnessStageNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserFitnessStageNotFoundException(Throwable cause) {
        super(cause);
    }

    public UserFitnessStageNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
