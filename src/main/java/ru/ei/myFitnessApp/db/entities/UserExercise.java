package ru.ei.myFitnessApp.db.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Time;

@Data
@Entity
@Table(name = "user_exercises")
public class UserExercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "number")
    private Integer number;

    @Column(name = "name")
    private String name;

    @Column(name = "missing")
    private Boolean missing;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_fitness_stage_id")
    private UserFitnessStage userFitnessStage;

    @Column(name = "execution_time")
    private Time executionTime;
}
