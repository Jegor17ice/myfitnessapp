package ru.ei.myFitnessApp.db.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class FitnessStage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "number", unique = true)
    private Integer number;

    @OneToMany(mappedBy = "fitnessStage", fetch = FetchType.EAGER)
    private List<Exercise> exercises;


}
