package ru.ei.myFitnessApp.db.entities;

public enum  ComplexityLevel {
    EASY, MIDDLE, HARD
}
