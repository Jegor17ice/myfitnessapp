package ru.ei.myFitnessApp.db.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User {
    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "complexity_level")
    private ComplexityLevel complexityLevel;

    @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<UserFitnessStage> userFitnessStages;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, ComplexityLevel complexityLevel) {
        this.username = username;
        this.password = password;
        this.complexityLevel = complexityLevel;
    }
}
