package ru.ei.myFitnessApp.db.entities;

public enum FitnessStageStatus {
    COMPLETED, CLOSE, IN_PROCESS
}
