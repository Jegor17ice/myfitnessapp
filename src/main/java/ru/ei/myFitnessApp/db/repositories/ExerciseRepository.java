package ru.ei.myFitnessApp.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ei.myFitnessApp.db.entities.Exercise;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
}
