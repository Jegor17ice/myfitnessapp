package ru.ei.myFitnessApp.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ei.myFitnessApp.db.entities.UserFitnessStage;

import java.util.List;

@Repository
public interface UserFitnessStageRepository extends JpaRepository<UserFitnessStage, Long> {
    @Query("SELECT u FROM UserFitnessStage u WHERE u.user =:userID")
    List<UserFitnessStage> findAllByUserID(@Param("userID") long userID);
}
