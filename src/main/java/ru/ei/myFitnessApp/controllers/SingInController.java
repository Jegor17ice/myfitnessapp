package ru.ei.myFitnessApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ei.myFitnessApp.services.UserService;

@Controller
public class SingInController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String getSingInPage() {
        return "log-in";
    }

    @PostMapping
    public String singIn(@RequestParam(name = "username") String name,
                         @RequestParam(name = "password") String password) {

        try {
            userService.authorize(name, password);
        } catch (Exception e) {
            return "error";
        }
        return "redirect:/stages";
    }
}
