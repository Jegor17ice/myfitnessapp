package ru.ei.myFitnessApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.ei.myFitnessApp.controllers.responses.UserExerciseResponse;
import ru.ei.myFitnessApp.exceptions.UserFitnessStageNotFoundException;
import ru.ei.myFitnessApp.services.UserFitnessStageService;
import ru.ei.myFitnessApp.services.exceptions.UserExerciseNotFoundException;

@Controller
public class UserExercisesController {

    @Autowired
    private UserFitnessStageService userFitnessStageService;

    @GetMapping("/user{user_id}/stage{stage_id}/start")
    public String getStartPage(@PathVariable(name = "user_id") Long userID,
                               @PathVariable(name = "stage_id") Long stageID,
                               Model model) {
        try {
            userFitnessStageService.getFirstExerciseResponse(stageID);
            return "start";
        } catch (UserFitnessStageNotFoundException e) {
            return "error";
        }
    }

    @GetMapping("/user{user_id}/stage{stage_id}/exercise-next{exercise_number}")
    public String getNextExercisePage(@PathVariable(name = "user_id") Long userID,
                                      @PathVariable(name = "stage_id") Long stageID,
                                      @PathVariable(name = "exercise_number") Integer exerciseNumber,
                                      Model model) {
        try {
            UserExerciseResponse response = userFitnessStageService.getNextUserExerciseResponseByNumber(stageID, exerciseNumber);
            return "nextExercise";


        } catch (UserExerciseNotFoundException e) {
            return "finish";
        } catch (UserFitnessStageNotFoundException e) {
            return "error";
        }

    }
}
