package ru.ei.myFitnessApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ei.myFitnessApp.services.UserService;

@Controller
@RequestMapping("/login")
public class LogInController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String getLogInPage() {
        return "login";
    }

    @PostMapping
    public String logIn(@RequestParam(name = "username") String name,
                        @RequestParam(name = "password") String password) {

        try {
            userService.register(name, password, "");
        } catch (Exception e) {
            System.out.println("!!!!!!!!!!!!!!!!!!!");
            return "error";
        }
        return "home";
    }
}
