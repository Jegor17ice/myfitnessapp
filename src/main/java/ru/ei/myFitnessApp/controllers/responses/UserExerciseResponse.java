package ru.ei.myFitnessApp.controllers.responses;

import lombok.Builder;
import lombok.Data;

import java.sql.Time;

@Data
@Builder
public class UserExerciseResponse {
    private Long id;
    private Long stageID;
    private String name;
    private String description;
    private Boolean missing;
    private Integer number;
    private Time executionTime;
}
