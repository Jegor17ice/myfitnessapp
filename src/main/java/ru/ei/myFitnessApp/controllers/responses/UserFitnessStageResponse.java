package ru.ei.myFitnessApp.controllers.responses;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserFitnessStageResponse {
    private Long stageID;
    private String name;
    private String description;
    private Long userID;
}
