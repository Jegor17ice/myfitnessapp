package ru.ei.myFitnessApp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ei.myFitnessApp.db.entities.*;
import ru.ei.myFitnessApp.db.repositories.FitnessStageRepository;
import ru.ei.myFitnessApp.db.repositories.UserFitnessStageRepository;
import ru.ei.myFitnessApp.db.repositories.UserRepository;
import ru.ei.myFitnessApp.services.exceptions.AuthorizationException;
import ru.ei.myFitnessApp.services.exceptions.RegistrationException;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserFitnessStageRepository userFitnessStageRepository;

    @Autowired
    private FitnessStageRepository fitnessStageRepository;


    public List<UserFitnessStage> getUserStages(long userID) {
        return userFitnessStageRepository.findAllByUserID(userID);
    }

    public User register(String username, String password, String complexityLevel) throws RegistrationException {
        try {

            User user = userRepository.findByUsername(username);

            if (user != null) {
                throw new RegistrationException();
            }

            //ComplexityLevel level = ComplexityLevelUtils.getComplexityLevel(complexityLevel);

            User userNew = new User(username, password, ComplexityLevel.EASY);
            user.setUserFitnessStages(getStages(fitnessStageRepository.findAll()));

            System.out.println(userNew.getUsername());
            System.out.println(userNew.getUserFitnessStages());
            userRepository.saveAndFlush(user);
            return user;

        } catch (Exception e) {
            throw new RegistrationException();
        }
    }

    private List<UserFitnessStage> getStages(List<FitnessStage> stages) {
        List<UserFitnessStage> result = new ArrayList<>();

        for (FitnessStage fitnessStage : stages) {
            System.out.println(fitnessStage.getName());
            UserFitnessStage stage = new UserFitnessStage();
            stage.setStatus(FitnessStageStatus.IN_PROCESS);
            stage.setName(fitnessStage.getName());
            stage.setDescription(fitnessStage.getDescription());
            stage.setNumber(fitnessStage.getNumber());

            List<UserExercise> exerciseList = new ArrayList<>();

            for (Exercise exercise : fitnessStage.getExercises()) {
                System.out.println(exercise.getId());
                UserExercise userExercise = new UserExercise();
                userExercise.setName(exercise.getName());
                userExercise.setDescription(exercise.getDescription());
                userExercise.setNumber(exercise.getNumber());
                userExercise.setExecutionTime(null);
            }

            stage.setUserExercises(exerciseList);
            result.add(stage);
        }

        return result;
    }

    public User authorize(String username, String password) throws AuthorizationException {
        try {
            User user = userRepository.findByUsername(username);

            if (user == null || !user.getPassword().equals(password)) {
                throw new AuthorizationException();
            }

            return user;
        } catch (AuthorizationException e) {
            throw new AuthorizationException();
        }
    }

}






