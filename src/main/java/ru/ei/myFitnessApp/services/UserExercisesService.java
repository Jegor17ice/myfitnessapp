package ru.ei.myFitnessApp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ei.myFitnessApp.db.entities.UserExercise;
import ru.ei.myFitnessApp.db.repositories.UserExerciseRepository;

import java.sql.Time;


@Service
public class UserExercisesService {
    @Autowired
    private UserExerciseRepository userExerciseRepository;

    public UserExercise getByID(long userExerciseID) {
        return userExerciseRepository.getOne(userExerciseID);
    }

    public void updateExecutionTime(Time time, long userExerciseID) {

    }
}
