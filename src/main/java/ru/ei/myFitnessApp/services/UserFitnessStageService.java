package ru.ei.myFitnessApp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ei.myFitnessApp.controllers.responses.UserExerciseResponse;
import ru.ei.myFitnessApp.controllers.responses.UserFitnessStageResponse;
import ru.ei.myFitnessApp.db.entities.FitnessStageStatus;
import ru.ei.myFitnessApp.db.entities.UserExercise;
import ru.ei.myFitnessApp.db.entities.UserFitnessStage;
import ru.ei.myFitnessApp.db.repositories.UserFitnessStageRepository;
import ru.ei.myFitnessApp.exceptions.UserFitnessStageNotFoundException;
import ru.ei.myFitnessApp.services.exceptions.UserExerciseNotFoundException;
import ru.ei.myFitnessApp.utils.ResponseUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserFitnessStageService {
    @Autowired
    private UserFitnessStageRepository userFitnessStageRepository;

    public UserFitnessStageResponse getStageResponse(long stageID) {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);

        return ResponseUtils.getStageResponse(stage);
    }

    public List<UserFitnessStageResponse> getAllUserStageResponse(long userID) {
        List<UserFitnessStageResponse> result = new ArrayList<>();

        for (UserFitnessStage stage : userFitnessStageRepository.findAllByUserID(userID)) {
            result.add(ResponseUtils.getStageResponse(stage));
        }

        return result;
    }

    public List<UserExercise> getAllUserExercisesByUserStageID(long id) {
        return null;
    }

    public void updateStatus(long userFitnessStageID, FitnessStageStatus status) throws UserFitnessStageNotFoundException {
        UserFitnessStage stage = userFitnessStageRepository.getOne(userFitnessStageID);

        if (stage == null) {
            throw new UserFitnessStageNotFoundException();
        }

        stage.setStatus(status);

        userFitnessStageRepository.save(stage);
    }

    public UserExerciseResponse getFirstExerciseResponse(long stageID) throws UserFitnessStageNotFoundException {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);
        if (stage == null) {
            throw new UserFitnessStageNotFoundException();
        }
        UserExercise exercise = stage.getFirstUserExercise();
        return ResponseUtils.getUserExerciseResponse(exercise);
    }

    public UserExerciseResponse getNextUserExerciseResponseByNumber(long stageID, int exerciseNumber) throws UserFitnessStageNotFoundException, UserExerciseNotFoundException {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);
        if (stage == null) {
            throw new UserFitnessStageNotFoundException();
        }

        exerciseNumber += 1;
        UserExercise exercise = stage.getUserExerciseByNumber(exerciseNumber);


        if (exercise == null) {
            throw new UserExerciseNotFoundException();
        }

        return ResponseUtils.getUserExerciseResponse(exercise);
    }
}
